# Auto Cover

Auto Cover is a script that you can use to create cover letters specifically tailored to employers.

## How to Use

requires: Python3.6+

```bash
git clone https://gitlab.com/Cyb3r-Jak3/auto-cover.git

cd auto-cover

pip install -r requirements.txt

cover.py
```

If you would rather only have the script:

```bash
curl https://gitlab.com/Cyb3r-Jak3/auto-cover/-/raw/master/cover.py

pip install python-docx comtypes

cover.py
```

## Available Options

Currently the only available replacement options are:

- COMPANY
- WEBSITE
- POSITION
- DATE

To add make the script replace them you need to add `<COMPANY>` to your cover letter.
