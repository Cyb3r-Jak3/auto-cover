"""Creates a PDF copy of resume"""
import os
from datetime import date
import comtypes.client
from docx import Document


today = date.today().strftime("%m/%d/%Y")


def text_replace(letter: Document(), answers: dict):
    """Seeks and replace text per section in a document"""
    sections = letter.paragraphs
    for section in sections:
        for replacement in answers.keys():
            if f"<{replacement}>" in section.text:
                section.text = section.text.replace(f"<{replacement}>",
                                                    answers[replacement])
    return letter


def covert_to_pdf(in_file):
    """Converts the word file to PDF"""
    word = comtypes.client.CreateObject('Word.Application')
    doc = word.Documents.Open(in_file)
    doc.SaveAs(f"{in_file}.pdf", FileFormat=17)
    doc.Close()
    word.Quit()


file_name = os.path.join(os.getcwd(),
                         input("Please enter the file name: "))
cover_letter = Document(file_name)
fill_ins = ["COMPANY", "POSITION", "WEBSITE"]  # , "RECRUITER"]
responses = {}
for item in fill_ins:
    responses[item] = input(f"Please the {item}: ")
cover_new = text_replace(cover_letter, responses)
cover_new.save("cover-new.docx")
covert_to_pdf(os.path.abspath("cover-new.docx"))
